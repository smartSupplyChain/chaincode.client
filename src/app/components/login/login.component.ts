import { Component, OnInit } from '@angular/core';
import { Configuration } from '../../configuration/configuration';
import { AuthentificationService } from '../../service/login/authentification.service';;
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RequestManagerService } from '../../service/request-manager/request-manager.service'
import { HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  serverAdress = Configuration.serves_adress.host;

  angForm!: FormGroup
  connectionStatus :boolean= true;
  user = {
    login: null,
    password: null
  };

  constructor(private AuthentificationService:AuthentificationService, private fb: FormBuilder,
     public router: Router, private RequestManagerService : RequestManagerService) {
    this.createForm();
  }

  title : any  = Configuration.titles.title;
  subtitle : any = Configuration.titles.subtitle;
  leftFooter : any = Configuration.titles.leftFooter;
  rightFooter : any = Configuration.titles.rightFooter;

  //not working
  createForm() {
    this.angForm = this.fb.group({
      email: ['', Validators.required ],
      password: ['', Validators.required ]
      });
  }

  async doLogin(angForm : any) { 
    let email  = angForm.value.email;
    let password = angForm.value.password;
  
    localStorage.clear();
    const headers = { 'Content-Type': 'application/json'};
    const authentBody = { 'email': email,  'password' : password};
    let authentUrl  = this.serverAdress + Configuration.authentication.auth;

    const accessTokenHttp$ =  this.AuthentificationService.getAccessToken(authentUrl, headers, authentBody);
    accessTokenHttp$
    .subscribe({
      next :(res) => {         
          let jsonStr: string = JSON.stringify(res);
          let jsonObj = JSON.parse(jsonStr);
          let token: string = jsonObj.access_token;
          localStorage.setItem('access_token',token);
          localStorage.setItem('email',email);
          
          this.connectionStatus = true;               
      },
       error : (err) => {
        console.log('getToken Error', err); 
        this.connectionStatus = false; 
       },
       complete: () => {
        let userDetailsUrl  = this.serverAdress + Configuration.userManager.userDetails;
        const access_token  = String(localStorage.getItem('access_token')); 
        const userDetailsBody = {'email': email};
        const headers = new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${access_token}`
        });
          
        const userDetailsHttp$ =  this.RequestManagerService.postRequest(userDetailsUrl, headers,userDetailsBody);
        userDetailsHttp$
        .subscribe({
          next :(res) => {     
               if(res.success){        
                let jsonStr: string = JSON.stringify(res.content);
                let jsonObj = JSON.parse(jsonStr);
            
                let login: string = jsonObj.login;
                let company: string = jsonObj.company;
                let type: string = jsonObj.type;
            
                // save user details in localStorage
                localStorage.setItem('login',login);
                localStorage.setItem('company',company);
                localStorage.setItem('type',type);
    
                this.router.navigate(['/home']); 
               }          
          },
           error : (err) => {
            console.log('getUserDetails Error', err);
            this.connectionStatus = false;          
           }
        });

       }
    });
  }

  ngOnInit() {
  }

}