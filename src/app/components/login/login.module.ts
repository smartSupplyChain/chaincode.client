import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';
import { AuthentificationService } from '../../service/login/authentification.service';
import {Routes,RouterModule} from '@angular/router';
import { FormsModule,ReactiveFormsModule} from '@angular/forms';

const routes: Routes = [
 { 
  path:'',component:LoginComponent 
 } 
];

@NgModule({
  declarations: [LoginComponent],
  providers : [AuthentificationService],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
	  RouterModule.forChild(routes) 
  ]
})
export class LoginModule { }
