import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {Routes,RouterModule} from '@angular/router';

import {ErrorsComponent} from '../errors/errors.component'

const routes: Routes = [
  { 
   path:'404',component:ErrorsComponent
  } ,
  { path:'**', redirectTo:'/404'} 
 ];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes) 
  ]
})
export class ErrorsModule { }
