import { Component } from '@angular/core';
import { CookieService } from 'ngx-cookie';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {

  user ={
    login : localStorage.getItem('login'),
    company : localStorage.getItem('company'),
    type : localStorage.getItem('type')
  }
  
  constructor(private cookies : CookieService) {}
 
  logout(){
    console.log("Logout ..")
    localStorage.clear();
  }

}
