import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { RequestManagerService } from '../../../../../service/request-manager/request-manager.service'
import { Configuration } from '../../../../../configuration/configuration';
import { HttpHeaders } from '@angular/common/http';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CustomSnackbarComponent } from 'src/app/components/tabs/utils/custom-snackbar/custom-snackbar.component';
@Component({
  selector: 'app-contract-create-edit',
  templateUrl: './contract-create-edit.component.html',
  styleUrls: ['./contract-create-edit.component.css']
})
export class ContractCreateEditComponent implements OnInit{

  dataSource!: MatTableDataSource<any>;
  contractForm!: FormGroup;
 
  serverAdress = Configuration.serves_adress.host;
  inprogress = false;
  title : any;
  editMode : boolean = false;
  users = [null];
  companies = [null];
  merchandises = [null];

  constructor(private RequestManagerService : RequestManagerService,
     private fbUser: FormBuilder,
     private _dialogRef : MatDialogRef<ContractCreateEditComponent>,
     @Inject(MAT_DIALOG_DATA) private data : any,
     private snackBar : MatSnackBar) {
        this.contractForm = this.fbUser.group({
        id: '',
        userClient : '',
        companyClient: '',
        merchandise_id: '',
        quantity: '',
        });
  }

  ngOnInit(): void {
    this.getAllCompanies();
    this.getAllMerchandises();
    this.getAllUsers();
    if(this.data == null){
      this.title = "Création de contract";
      this.data = { 
        'userClient': localStorage.getItem('login'),
        'companyClient': localStorage.getItem('company')
      }
    }
    else {
      this.title = "Modification du contract : " + this.data.id;
      this.editMode = true;
    }
    this.contractForm.patchValue(this.data);
  }

  /**
  * get all companies
  */
  getAllCompanies(){
    let allCompniesUrl  = this.serverAdress + Configuration.companyManager.getAllCompanies;
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('access_token')}`
    });
    let body = { 'email': localStorage.getItem('email')};
    const companiesHttp$ =  this.RequestManagerService.postRequest(allCompniesUrl, headers, body);
    companiesHttp$
    .subscribe({
      next :(res) => {      
           if(res.success){        
             this.populateCompanies(res.content);
           }          
      },
       error(err) {
        console.log('HTTP Error', err)
       }
    })
  }

  /**
  * get all merchandises
  */
  getAllMerchandises(){
      let allMerchandisesUrl  = this.serverAdress + Configuration.merchandiseManager.getAllMerchandises;
      let headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('access_token')}`
      });
      let body = { 'email': localStorage.getItem('login')};
      const merchandisesHttp$ =  this.RequestManagerService.postRequest(allMerchandisesUrl, headers, body);
      merchandisesHttp$
      .subscribe({
        next :(res) => {      
             if(res.success){            
              this.populateMerchandises(res.content);
             }          
        },
         error(err) {
          console.log('HTTP Error', err)
         }
      })
  }
   
  /**
  * get all users
  */
  getAllUsers(){
    let allUsersUrl  = this.serverAdress + Configuration.userManager.getAllusers;
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('access_token')}`
    });
    let body = { 'email': localStorage.getItem('login')};
    const companiesHttp$ =  this.RequestManagerService.postRequest(allUsersUrl, headers, body);
    companiesHttp$
    .subscribe({
      next :(res) => {      
           if(res.success){    
            this.populateUsers(res.content);
           }          
      },
       error(err) {
        console.log('HTTP Error', err)
       }
    })
  }

  /**
  * populaute users
  */
  populateUsers(res : any) {
    for (var val of res) {
      this.users.push(val.login);
    } 
  }

  /**
  * populaute companies
  */
  populateCompanies(res : any) {
    for (var val of res) {
      this.companies.push(val.id);
    } 
  }

  /**
  * populaute users
  */
  populateMerchandises(res : any) {
    for (var val of res) {
      this.merchandises.push(val.id);
    } 
  }

  /**
  * create or edit contract
  */
  createEditContract(contractForm : any) { 
    if(this.editMode){
      this.editContract(contractForm);
    }
    else {
      this.createContract(contractForm);
    } 
  }

  /**
  * cancel create, edit form
  */
   formCancel() {
    this._dialogRef.close();
  }
  
  /**
   *  create contract
   */
  createContract(addContractForm : any) { 
    let id  = addContractForm.value.id;
    let userClient = addContractForm.value.userClient;
    let companyClient  = addContractForm.value.companyClient;
    let merchandise_id  = addContractForm.value.merchandise_id;
    let quantity  = addContractForm.value.quantity;
      
    if(Boolean(id)  && Boolean(userClient) && Boolean(companyClient) 
      && Boolean(merchandise_id) && Boolean(quantity)){ 
      this.inprogress = true;
      let createContractUrl  = this.serverAdress +  `${Configuration.contractManager.createContract}`;
      let headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('access_token')}`
      });
      let body = { 
        'id': id,
        'userClient': userClient,
        'companyClient': companyClient,
        'merchandise_id':merchandise_id,
        'quantity': quantity 
    };
      const createContractHttp$ =  this.RequestManagerService.createRequest(createContractUrl, body, headers);
      createContractHttp$
      .subscribe({
        next :(res) => {      
          this.inprogress = false;

          let isSuccess  = false;
          let panelClassToApply = 'error-message';
          if(res.success){   
            this._dialogRef.close();
            isSuccess = true;
            panelClassToApply = 'success-message';
          } 
          
          this.snackBar.openFromComponent(CustomSnackbarComponent,{
            data : {
              message :res.message,
              success : isSuccess
            },
            duration : (5000),
            panelClass : panelClassToApply
          });     
        },
         error(err) {
          console.log('HTTP Error', err);
         }
      })       
    }   
  }

  /**
  *  edit contract 
  */
  editContract(editContractForm : any) { 
    let id  = editContractForm.value.id;
    let userClient = editContractForm.value.userClient;
    let companyClient  = editContractForm.value.companyClient;
    let merchandise_id  = editContractForm.value.merchandise_id;
    let quantity  = editContractForm.value.quantity;

    if(Boolean(id)  && Boolean(userClient) && Boolean(companyClient) 
      && Boolean(merchandise_id) && Boolean(quantity)){ 
      this.inprogress = true;
      let editContratUrl  = this.serverAdress +  `${Configuration.contractManager.updateContract}`;
      let headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('access_token')}`
      });
      let body = { 
        'id': id,
        'userClient': userClient,
        'companyClient': companyClient,
        'merchandise_id':merchandise_id,
        'quantity': quantity,
        'userSupplier': '',
        'companySupplier': '',
        'stock_id' : '',
        'status' : '' 
    };
      const editContratHttp$ =  this.RequestManagerService.putRequest(editContratUrl, body, headers);
      editContratHttp$
      .subscribe({
        next :(res) => {
          this.inprogress = false; 

          let isSuccess  = false;
          let panelClassToApply = 'error-message';
          if(res.success){   
            this._dialogRef.close();
            isSuccess = true;
            panelClassToApply = 'success-message';  
          } 
          
          this.snackBar.openFromComponent(CustomSnackbarComponent,{
            data : {
              message :res.message,
              success : isSuccess
            },
            duration : (5000),
            panelClass : panelClassToApply
          });       
        },
         error(err) {
          console.log('HTTP Error', err);
         }
      })       
    }    
  }
}
