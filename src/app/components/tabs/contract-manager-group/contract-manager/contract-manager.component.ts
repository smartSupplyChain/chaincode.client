import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { RequestManagerService } from '../../../../service/request-manager/request-manager.service'
import { Configuration } from '../../../../configuration/configuration';
import { HttpHeaders } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CustomSnackbarComponent } from '../../utils/custom-snackbar/custom-snackbar.component';
import { ContractCreateEditComponent } from './contract-create-edit/contract-create-edit.component';
@Component({
  selector: 'app-contract-manager',
  templateUrl: './contract-manager.component.html',
  styleUrls: ['./contract-manager.component.css']
})
export class ContractManagerComponent implements OnInit {
  displayedColumns: string[] = ['Nom contrat', 'Marchandise', 'Quantité', 'Utilisateur', 'Entreprise', 'Delete', 'Edit'];
  dataSource!: MatTableDataSource<any>;

  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  inprogress = false;
  serverAdress = Configuration.serves_adress.host;
  currentUser = localStorage.getItem('email');

  constructor(private RequestManagerService : RequestManagerService, private _dialog: MatDialog, private snackBar : MatSnackBar) {
  }
  
  ngOnInit(): void {
    this.getAllContracts();

    this.paginator._intl.itemsPerPageLabel = 'Lignes par page'; 
    this.paginator._intl.nextPageLabel = 'page suivante';
    this.paginator._intl.previousPageLabel = 'page précédente';
    this.paginator._intl.getRangeLabel = (page: number, pageSize: number, length: number) => {
      if (length === 0 || pageSize === 0) {
        return `0 à ${length }`;
      }
      length = Math.max(length, 0);
      const startIndex = page * pageSize;
      const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;
      return `${startIndex + 1} à ${endIndex} sur ${length}`;
    };     
  }

  /**
  * get all contracts
  */
  getAllContracts(){
    let allContractsUrl  = this.serverAdress + Configuration.contractManager.getAllContracts;
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('access_token')}`
    });
    let body = { 'email': localStorage.getItem('login')};
    const companiesHttp$ =  this.RequestManagerService.postRequest(allContractsUrl, headers, body);
    companiesHttp$
    .subscribe({
      next :(res) => {        
           if(res.success){  
            this.dataSource = new MatTableDataSource(res.content);
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;
           }          
      },
       error(err) {
        console.log('HTTP Error', err)
       }
    })
  }

  /**
  * delete contract
  * @param email
  */
  deleteContract(contractId : any){
    this.inprogress = true;
    let deleteContractUrl  = this.serverAdress + `${Configuration.contractManager.deleteContract}${contractId}`;
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('access_token')}`
    });
    const deleteContractHttp$ =  this.RequestManagerService.deleteRequest(deleteContractUrl, headers);
    deleteContractHttp$
    .subscribe({
      next :(res) => { 
        this.inprogress = false; 
        let isSuccess  = false;
        let panelClassToApply = 'error-message';

        if(res.success){           
          this.getAllContracts(); 
          isSuccess = true;
          panelClassToApply = 'success-message';
        } 
        this.snackBar.openFromComponent(CustomSnackbarComponent,{
          data : {
            message :res.message,
            success : isSuccess
          },
          duration : (5000),
          panelClass : panelClassToApply
        });          
      },
       error(err) {
        console.log('HTTP Error', err)
       }
    })
  }

  /**
  * open edit contract from
  * @param data
  */
  openEditContractForm(data : any){
   const dialogRef = this._dialog.open(ContractCreateEditComponent, {data});
   dialogRef.afterClosed().subscribe({
    next:(val) => {
      this.getAllContracts();
    }
   })
  }

  /**
  * open create contract from
  */
   openCreateContractForm(){
    const dialogRef = this._dialog.open(ContractCreateEditComponent);
    dialogRef.afterClosed().subscribe({
     next:(val) => {
       this.getAllContracts();
     }
    })
   }

  /**
   * paginator
   * @param event 
   */
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
