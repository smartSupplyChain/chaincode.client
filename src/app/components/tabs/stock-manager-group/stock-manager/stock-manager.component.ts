import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { RequestManagerService } from '../../../../service/request-manager/request-manager.service'
import { Configuration } from '../../../../configuration/configuration';
import { HttpHeaders } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { StockCreateEditComponent } from './stock-create-edit/stock-create-edit.component';
import { CustomSnackbarComponent } from '../../utils/custom-snackbar/custom-snackbar.component';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-stock-manager',
  templateUrl: './stock-manager.component.html',
  styleUrls: ['./stock-manager.component.css']
})
export class StockManagerComponent implements OnInit{
  displayedColumns: string[] = ['Nom des stocks', 'Marchandise', 'Quantité', 'Utilisateur', 'Entreprise', 'Delete', 'Edit'];
  dataSource!: MatTableDataSource<any>;

  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  inprogress = false;
  serverAdress = Configuration.serves_adress.host;
  currentUser = localStorage.getItem('email');

  constructor(private RequestManagerService : RequestManagerService, private _dialog: MatDialog, private snackBar : MatSnackBar) {
  }
  
  ngOnInit(): void {
    this.getAllStocks(); 

    this.paginator._intl.itemsPerPageLabel = 'Lignes par page'; 
    this.paginator._intl.nextPageLabel = 'page suivante';
    this.paginator._intl.previousPageLabel = 'page précédente';
    this.paginator._intl.getRangeLabel = (page: number, pageSize: number, length: number) => {
      if (length === 0 || pageSize === 0) {
        return `0 à ${length }`;
      }
      length = Math.max(length, 0);
      const startIndex = page * pageSize;
      const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;
      return `${startIndex + 1} à ${endIndex} sur ${length}`;
    };     
  }

  /**
  * get all stocks
  */
  getAllStocks(){
    let allStocksUrl  = this.serverAdress + Configuration.stockManager.getAllStocks;
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('access_token')}`
    });
    let body = { 'email': localStorage.getItem('login')};
    const stocksHttp$ =  this.RequestManagerService.postRequest(allStocksUrl, headers, body);
    stocksHttp$
    .subscribe({
      next :(res) => {      
           if(res.success){            
            this.dataSource = new MatTableDataSource(res.content);
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;
           }          
      },
       error(err) {
        console.log('HTTP Error', err)
       }
    })
  }

  /**
  * delete stock
  * @param id
  */
  deleteStock(id : any){
    this.inprogress = true;
    let deleteUserUrl  = this.serverAdress + `${Configuration.stockManager.deleteStock}${id}`;
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('access_token')}`
    });
    const deleteStockHttp$ =  this.RequestManagerService.deleteRequest(deleteUserUrl, headers);
    deleteStockHttp$
    .subscribe({
      next :(res) => { 
           this.inprogress = false; 

           let isSuccess  = false;
           let panelClassToApply = 'error-message'; 
           if(res.success){          
            this.getAllStocks();  
            isSuccess = true;
            panelClassToApply = 'success-message'; 
           }
           
           this.snackBar.openFromComponent(CustomSnackbarComponent,{
            data : {
              message :res.message,
              success : isSuccess
            },
            duration : (5000),
            panelClass : panelClassToApply
          });           
      },
       error(err) {
        console.log('HTTP Error', err)
       }
    })
  }

  /**
  * open edit stock from
  * @param data
  */
  openEditStockForm(data : any){
   const dialogRef = this._dialog.open(StockCreateEditComponent, {data});
   dialogRef.afterClosed().subscribe({
    next:(val) => {
      this.getAllStocks();
    }
   })
  }

  /**
  * open create stock from
  */
  openCreateStockForm(){
    const dialogRef = this._dialog.open(StockCreateEditComponent);
    dialogRef.afterClosed().subscribe({
     next:(val) => {
       this.getAllStocks();
     }
    })
  }

  /**
  * paginator
  * @param event 
  */
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
