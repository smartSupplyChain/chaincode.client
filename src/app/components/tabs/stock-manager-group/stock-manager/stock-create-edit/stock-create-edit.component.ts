import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { RequestManagerService } from '../../../../../service/request-manager/request-manager.service'
import { Configuration } from '../../../../../configuration/configuration';
import { HttpHeaders } from '@angular/common/http';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CustomSnackbarComponent } from 'src/app/components/tabs/utils/custom-snackbar/custom-snackbar.component';

@Component({
  selector: 'app-stock-create-edit',
  templateUrl: './stock-create-edit.component.html',
  styleUrls: ['./stock-create-edit.component.css']
})
export class StockCreateEditComponent implements OnInit {

  dataSource!: MatTableDataSource<any>;
  stockForm!: FormGroup;
 
  serverAdress = Configuration.serves_adress.host;
  inprogress = false;
  title : any;
  editMode : boolean = false;
  companySuppliers = [null];
  userSuppliers = [null];
  merchandises = [null];

  constructor(private RequestManagerService : RequestManagerService,
     private fbStock: FormBuilder,
     private _dialogRef : MatDialogRef<StockCreateEditComponent>,
     @Inject(MAT_DIALOG_DATA) private data : any,
     private snackBar : MatSnackBar) {
        this.stockForm = this.fbStock.group({
        userSupplier: '',
        companySupplier: '',
        id: '',
        merchandiseId: '',
        quantity: '',
        });
  }

  ngOnInit(): void {
    this.getAllUsers();
    this.getAllCompanies();
    this.getAllMerchandises();

    if(this.data == null){
       this.title = "Création de stock";
       this.data = { 
        'userSupplier': localStorage.getItem('login'),
        'companySupplier': localStorage.getItem('company')
      }
    }
    else {
      this.title = "Modification du stock : " + this.data.id;
      this.editMode = true; 
    }
    this.stockForm.patchValue(this.data);
  }

  /**
  * get all merchandises
  */
  getAllMerchandises(){
    let allMerchandisesUrl  = this.serverAdress + Configuration.merchandiseManager.getAllMerchandises;
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('access_token')}`
    });
    let body = { 'email': localStorage.getItem('login')};
    const merchandisesHttp$ =  this.RequestManagerService.postRequest(allMerchandisesUrl, headers, body);
    merchandisesHttp$
    .subscribe({
      next :(res) => {      
           if(res.success){            
            this.populateMerchandises(res.content);
           }          
      },
       error(err) {
        console.log('HTTP Error', err)
       }
    })
}

 /**
  * populaute user userSuppliers
  */
 populateMerchandises(res : any) {
  for (var val of res) {
    this.merchandises.push(val.id);
  } 
}
  /**
  * get all users
  */
  getAllUsers(){
      let allUsersUrl  = this.serverAdress + Configuration.userManager.getAllusers;
      let headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('access_token')}`
      });
      let body = { 'email': localStorage.getItem('login')};
      const companiesHttp$ =  this.RequestManagerService.postRequest(allUsersUrl, headers, body);
      companiesHttp$
      .subscribe({
        next :(res) => {      
             if(res.success){    
              this.populateUserSuppliers(res.content);
             }          
        },
         error(err) {
          console.log('HTTP Error', err)
         }
      })
  }

  /**
  * populaute user userSuppliers
  */
   populateUserSuppliers(res : any) {
    for (var val of res) {
      this.userSuppliers.push(val.login);
    } 
  }

  /**
  * get all companies
  */
  getAllCompanies(){
    let allCompniesUrl  = this.serverAdress + Configuration.companyManager.getAllCompanies;
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('access_token')}`
    });
    let body = { 'email': localStorage.getItem('email')};
    const companiesHttp$ =  this.RequestManagerService.postRequest(allCompniesUrl, headers, body);
    companiesHttp$
    .subscribe({
      next :(res) => {      
           if(res.success){            
            this.populateCompanies(res.content);
           }          
      },
       error(err) {
        console.log('HTTP Error', err)
       }
    })
  }

  /**
  * populaute user companies
  */
  populateCompanies(res : any) {
    for (var val of res) {
      this.companySuppliers.push(val.id);
    } 
  }

  /**
  * create or edit stock
  */
  createEditStock(stockForm : any) { 
    if(this.editMode){ 
      this.editStock(stockForm); 
    }
    else {
      this.createStock(stockForm);
    } 
  }

  /**
   *  create stock
   */
  createStock(addUserForm : any) { 
    let userSupplier  = addUserForm.value.userSupplier;
    let companySupplier  = addUserForm.value.companySupplier;
    let id  = addUserForm.value.id;
    let merchandiseId  = addUserForm.value.merchandiseId;
    let quantity = addUserForm.value.quantity;
    
    if(Boolean(userSupplier) && Boolean(companySupplier) && Boolean(id)
      && Boolean(merchandiseId) && Boolean(quantity)){    
      this.inprogress = true;
      let createUserUrl  = this.serverAdress +  `${Configuration.stockManager.createStock}`;
      let headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('access_token')}`
      });
      let body = { 
        'userSupplier': userSupplier,
        'companySupplier': companySupplier,
        'id': id,
        'merchandiseId':merchandiseId,
        'quantity': quantity 
    };
      const createStockHttp$ =  this.RequestManagerService.createRequest(createUserUrl, body, headers);
      createStockHttp$
      .subscribe({
        next :(res) => {  
          this.inprogress = false; 

          let isSuccess  = false;
          let panelClassToApply = 'error-message';
          if(res.success){  
            this._dialogRef.close();    
            isSuccess = true;
            panelClassToApply = 'success-message';
          } 
          
          this.snackBar.openFromComponent(CustomSnackbarComponent,{
            data : {
              message :res.message,
              success : isSuccess
            },
            duration : (5000),
            panelClass : panelClassToApply
          });      
        },
         error(err) {
          console.log('HTTP Error', err);
         }
      })
    }   
  }

  /**
  *  edit stock 
  */
  editStock(editStockFormForm : any) {   //TODO
    let userSupplier  = editStockFormForm.value.userSupplier;
    let companySupplier  = editStockFormForm.value.companySupplier;
    let id  = editStockFormForm.value.id;
    let merchandiseId  = editStockFormForm.value.merchandiseId;
    let quantity = editStockFormForm.value.quantity;

    if(Boolean(userSupplier) && Boolean(companySupplier) && Boolean(id)
      && Boolean(merchandiseId) && Boolean(quantity)){  

      this.inprogress = true;
      let updateUserUrl  = this.serverAdress +  `${Configuration.stockManager.updateStock}`;
      let headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('access_token')}`
      });
      let body = { 
        'userSupplier': userSupplier,
        'companySupplier': companySupplier,
        'id': id,
        'merchandiseId':merchandiseId,
        'quantity': quantity 
    };
      const updateStockHttp$ =  this.RequestManagerService.putRequest(updateUserUrl, body, headers);
      updateStockHttp$
      .subscribe({
        next :(res) => {
          this.inprogress = false; 

           let isSuccess  = false;
           let panelClassToApply = 'error-message';
          if(res.success){   
            this._dialogRef.close();    
            isSuccess = true;
            panelClassToApply = 'success-message';
          }
          
          this.snackBar.openFromComponent(CustomSnackbarComponent,{
            data : {
              message :res.message,
              success : isSuccess
            },
            duration : (5000),
            panelClass : panelClassToApply // color not working
          });        
        },
         error(err) {
          console.log('HTTP Error', err);
         }
      })
    } 
  }

    /**
  * cancel create, edit form
  */
    formCancel() {
      this._dialogRef.close();
    }
  
}
