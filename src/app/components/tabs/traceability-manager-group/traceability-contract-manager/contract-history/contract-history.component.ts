import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { RequestManagerService } from '../../../../../service/request-manager/request-manager.service'
import { Configuration } from '../../../../../configuration/configuration';
import { HttpHeaders } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-contract-history',
  templateUrl: './contract-history.component.html',
  styleUrls: ['./contract-history.component.css']
})
export class ContractHistoryComponent implements OnInit{
  displayedColumns: string[] = ['Blockchain Date', 'Blockchain Status', 'Tx', 'Application Date', 'quantity', 'Status'];
  dataSource!: MatTableDataSource<any>;

  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  inprogress = false;
  serverAdress = Configuration.serves_adress.host;
  userId : any;
  constructor(private RequestManagerService : RequestManagerService, private _dialog: MatDialog, private _dialogRef : MatDialogRef<ContractHistoryComponent>, @Inject(MAT_DIALOG_DATA) private data : any ) {
  this.userId = data;
  }
  
  ngOnInit(): void {
    this.getContactHistory(); 

    this.paginator._intl.itemsPerPageLabel = 'Lignes par page'; 
    this.paginator._intl.nextPageLabel = 'page suivante';
    this.paginator._intl.previousPageLabel = 'page précédente';
    this.paginator._intl.getRangeLabel = (page: number, pageSize: number, length: number) => {
      if (length === 0 || pageSize === 0) {
        return `0 à ${length }`;
      }
      length = Math.max(length, 0);
      const startIndex = page * pageSize;
      const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;
      return `${startIndex + 1} à ${endIndex} sur ${length}`;
    };     
  }

  /**
  * get stock history
  */
  getContactHistory(){
    let historyUrl  = this.serverAdress + Configuration.historyManager.contractHistory;
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('access_token')}`
    });
    let body = { 'id': this.userId};
    const stocksHttp$ =  this.RequestManagerService.postRequest(historyUrl, headers, body);
    stocksHttp$
    .subscribe({
      next :(res) => {    
           if(res.success){
            for(let element of res.content) {
              element.timestamp = element.timestamp.replace('janvier', 'jan.').replace('février', 'fev.')
              .replace('avril', 'avr.').replace('juin', 'jui.').replace('octobre', 'oct.')
              .replace('novembre', 'nov.').replace('décembre', 'dec.');
            }          
            this.dataSource = new MatTableDataSource(res.content);
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;
           }          
      },
       error(err) {
        console.log('HTTP Error', err)
       }
    })
  }
}
