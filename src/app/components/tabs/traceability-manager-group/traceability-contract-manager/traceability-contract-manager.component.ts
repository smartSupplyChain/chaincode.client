import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { RequestManagerService } from '../../../../service/request-manager/request-manager.service'
import { Configuration } from '../../../../configuration/configuration';
import { HttpHeaders } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { ContractHistoryComponent } from '../traceability-contract-manager/contract-history/contract-history.component'

@Component({
  selector: 'app-traceability-contract-manager',
  templateUrl: './traceability-contract-manager.component.html',
  styleUrls: ['./traceability-contract-manager.component.css']
})

export class TraceabilityContractManagerComponent  implements OnInit{
  contractDisplayedColumns: string[] = ['Nom contrat', 'Utilisateur', 'Entreprise', 'Marchandise', 'View'];
  contractDataSource!: MatTableDataSource<any>;

  contractStatusDisplayedColumns: string[] = ['Date', 'Quantity', 'Status'];
  contractStatusDataSource!: MatTableDataSource<any>;

  contractSupplierDisplayedColumns: string[] = ['Nom stock', 'Utilisateur', 'Entreprise'];
  contractSupplierDataSource!: MatTableDataSource<any>;

  @ViewChild('paginatorContract', { static: true }) paginatorContract!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  inprogress = false;
  serverAdress = Configuration.serves_adress.host;
  currentUser = localStorage.getItem('email');

  constructor(private RequestManagerService : RequestManagerService, private _dialog: MatDialog) {
  }
  
  ngOnInit(): void {
    this.getAllContracts(); 

    this.paginatorContract._intl.itemsPerPageLabel = 'Lignes par page'; 
    this.paginatorContract._intl.nextPageLabel = 'page suivante';
    this.paginatorContract._intl.previousPageLabel = 'page précédente';
    this.paginatorContract._intl.getRangeLabel = (page: number, pageSize: number, length: number) => {
      if (length === 0 || pageSize === 0) {
        return `0 à ${length }`;
      }
      length = Math.max(length, 0);
      const startIndex = page * pageSize;
      const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;
      return `${startIndex + 1} à ${endIndex} sur ${length}`;
    };
  }

  /**
   * open  contract history form
   */
  openViewContractHistoryForm(data : any){
    const dialogRef = this._dialog.open(ContractHistoryComponent, {data});
       dialogRef.afterClosed().subscribe({
        next:(val) => {
        }
      })
  }
   
   /**
  * get all contracts
  */
   getAllContracts(){
    let allContractsUrl  = this.serverAdress + Configuration.contractManager.getAllContracts;
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('access_token')}`
    });
    let body = { 'email': localStorage.getItem('login')};
    const companiesHttp$ =  this.RequestManagerService.postRequest(allContractsUrl, headers, body);
    companiesHttp$
    .subscribe({
      next :(res) => {        
           if(res.success){  
            let dataSource = new MatTableDataSource(res.content);
            // identifications des contrats
            this.contractDataSource = dataSource;
            this.contractDataSource.sort = this.sort;
            this.contractDataSource.paginator = this.paginatorContract;

            // statut des contrats
            this.contractStatusDataSource = dataSource;
            this.contractStatusDataSource.sort = this.sort;
            this.contractStatusDataSource.paginator = this.paginatorContract;

            // information fournisseurs
            this.contractSupplierDataSource = dataSource;
            this.contractSupplierDataSource.sort = this.sort;
            this.contractSupplierDataSource.paginator = this.paginatorContract;
          }          
      },
       error(err) {
        console.log('HTTP Error', err)
       }
    })
  }

  /**
   * contract paginator 
   * @param event 
   */
  applyContractFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.contractDataSource.filter = filterValue.trim().toLowerCase();
    if (this.contractDataSource.paginator) {
      this.contractDataSource.paginator.firstPage();
    }
  }

  /**
   * contract status paginator 
   * @param event 
   */
  applyContractStatusFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.contractStatusDataSource.filter = filterValue.trim().toLowerCase();
    if (this.contractStatusDataSource.paginator) {
      this.contractStatusDataSource.paginator.firstPage();
    }
  }

  /**
   * contract status paginator 
   * @param event 
   */
  applyContractSupplierFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.contractSupplierDataSource.filter = filterValue.trim().toLowerCase();
    if (this.contractSupplierDataSource.paginator) {
      this.contractSupplierDataSource.paginator.firstPage();
    }
  }

}