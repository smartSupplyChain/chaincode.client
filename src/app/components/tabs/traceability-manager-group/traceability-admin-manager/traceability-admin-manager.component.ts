import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { RequestManagerService } from '../../../../service/request-manager/request-manager.service'
import { Configuration } from '../../../../configuration/configuration';
import { HttpHeaders } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { UserHistoryComponent } from '../traceability-admin-manager/user-history/user-history.component'
@Component({
  selector: 'app-traceability-admin-manager',
  templateUrl: './traceability-admin-manager.component.html',
  styleUrls: ['./traceability-admin-manager.component.css']
})

export class TraceabilityAdminManagerComponent  implements OnInit{
  userDisplayedColumns: string[] = ['Utilisateur', 'Entreprise', 'Profil', 'View'];
  userDataSource!: MatTableDataSource<any>;

  companyDisplayedColumns: string[] = ['Id', 'Date'];
  companyDataSource!: MatTableDataSource<any>;

  merchandiseDisplayedColumns: string[] = ['Id', 'Date'];
  merchandiseDataSource!: MatTableDataSource<any>;

  @ViewChild('paginatorUsers', { static: true }) paginatorUsers!: MatPaginator;
  @ViewChild('paginatorCompanies', { static: true }) paginatorCompanies!: MatPaginator;
  @ViewChild('paginatorMerchandises', { static: true }) paginatorMerchandises!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  inprogress = false;
  serverAdress = Configuration.serves_adress.host;
  currentUser = localStorage.getItem('email');

  constructor(private RequestManagerService : RequestManagerService, private _dialog: MatDialog) {
  }
  
  ngOnInit(): void {
    this.getAllUsers(); 
    this.getAllCompanies(); 
    this.getMerchandises(); 

    this.paginatorUsers._intl.itemsPerPageLabel = 'Lignes par page'; 
    this.paginatorUsers._intl.nextPageLabel = 'page suivante';
    this.paginatorUsers._intl.previousPageLabel = 'page précédente';
    this.paginatorUsers._intl.getRangeLabel = (page: number, pageSize: number, length: number) => {
      if (length === 0 || pageSize === 0) {
        return `0 à ${length }`;
      }
      length = Math.max(length, 0);
      const startIndex = page * pageSize;
      const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;
      return `${startIndex + 1} à ${endIndex} sur ${length}`;
    };

    this.paginatorCompanies._intl.itemsPerPageLabel = 'Lignes par page'; 
    this.paginatorCompanies._intl.nextPageLabel = 'page suivante';
    this.paginatorCompanies._intl.previousPageLabel = 'page précédente';
    this.paginatorCompanies._intl.getRangeLabel = (page: number, pageSize: number, length: number) => {
      if (length === 0 || pageSize === 0) {
        return `0 à ${length }`;
      }
      length = Math.max(length, 0);
      const startIndex = page * pageSize;
      const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;
      return `${startIndex + 1} à ${endIndex} sur ${length}`;
    };
         
    this.paginatorMerchandises._intl.itemsPerPageLabel = 'Lignes par page'; 
    this.paginatorMerchandises._intl.nextPageLabel = 'page suivante';
    this.paginatorMerchandises._intl.previousPageLabel = 'page précédente';
    this.paginatorMerchandises._intl.getRangeLabel = (page: number, pageSize: number, length: number) => {
      if (length === 0 || pageSize === 0) {
        return `0 à ${length }`;
      }
      length = Math.max(length, 0);
      const startIndex = page * pageSize;
      const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;
      return `${startIndex + 1} à ${endIndex} sur ${length}`;
    };     
  }

  /**
   * open  user history form
   */
  openViewUserHistoryForm(data : any){
       const dialogRef = this._dialog.open(UserHistoryComponent, {data});
       dialogRef.afterClosed().subscribe({
        next:(val) => {
          this.getAllUsers();
        }
       })
  }
   
  /**
   * get all users
   */
  getAllUsers(){
    let allUsersUrl  = this.serverAdress + Configuration.userManager.getAllusers;
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('access_token')}`
    });
    let body = { 'email': localStorage.getItem('login')};
    const companiesHttp$ =  this.RequestManagerService.postRequest(allUsersUrl, headers, body);
    companiesHttp$
    .subscribe({
      next :(res) => {        
           if(res.success){  
            this.userDataSource = new MatTableDataSource(res.content);
            this.userDataSource.sort = this.sort;
            this.userDataSource.paginator = this.paginatorUsers;
           }          
      },
       error(err) {
        console.log('HTTP Error', err)
       }
    })
  }

  /**
   * get all companies
   */
  getAllCompanies(){
    let allCompniesUrl  = this.serverAdress + Configuration.companyManager.getAllCompanies;
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('access_token')}`
    });
    let body = { 'email': localStorage.getItem('email')};
    const companiesHttp$ =  this.RequestManagerService.postRequest(allCompniesUrl, headers, body);
    companiesHttp$
    .subscribe({
      next :(res) => {      
           if(res.success){        
              this.companyDataSource = new MatTableDataSource(res.content);
              this.companyDataSource.sort = this.sort;
              this.companyDataSource.paginator = this.paginatorCompanies;
           }          
      },
       error(err) {
        console.log('HTTP Error', err)
       }
    })
  }

  /**
   * get all merchandises
   */
  getMerchandises(){
  let allMerchandiseUrl  = this.serverAdress + Configuration.merchandiseManager.getAllMerchandises;
  let headers = new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${localStorage.getItem('access_token')}`
  });
  let body = { 'email': localStorage.getItem('email')};
  const merchandisesHttp$ =  this.RequestManagerService.postRequest(allMerchandiseUrl, headers, body);
  merchandisesHttp$
  .subscribe({ 
    next :(res) => {      
         if(res.success){       
            this.merchandiseDataSource = new MatTableDataSource(res.content);
            this.merchandiseDataSource.sort = this.sort;
            this.merchandiseDataSource.paginator = this.paginatorMerchandises;
         }          
    },
     error(err) {
      console.log('HTTP Error', err)
     }
  })
  }

  /**
   * user paginator 
   * @param event 
   */
  applyUserFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.userDataSource.filter = filterValue.trim().toLowerCase();
    if (this.userDataSource.paginator) {
      this.userDataSource.paginator.firstPage();
    }
  }

  /**
   * company paginator 
   * @param event 
   */
  applyCompanyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.companyDataSource.filter = filterValue.trim().toLowerCase();
    if (this.companyDataSource.paginator) {
      this.companyDataSource.paginator.firstPage();
    }
  }

  /**
   * merchandise paginator 
   * @param event 
   */
  applyMerchandiseFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.merchandiseDataSource.filter = filterValue.trim().toLowerCase();
    if (this.merchandiseDataSource.paginator) {
      this.merchandiseDataSource.paginator.firstPage();
    }
  }

}