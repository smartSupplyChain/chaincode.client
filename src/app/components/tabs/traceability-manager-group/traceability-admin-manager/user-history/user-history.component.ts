import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { RequestManagerService } from '../../../../../service/request-manager/request-manager.service'
import { Configuration } from '../../../../../configuration/configuration';
import { HttpHeaders } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-user-history',
  templateUrl: './user-history.component.html',
  styleUrls: ['./user-history.component.css']
})
export class UserHistoryComponent implements OnInit{
  displayedColumns: string[] = ['Blockchain Date', 'Status', 'Tx', 'Application Date', 'Company', 'profil'];
  dataSource!: MatTableDataSource<any>;

  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  inprogress = false;
  serverAdress = Configuration.serves_adress.host;
  userId : any;
  constructor(private RequestManagerService : RequestManagerService, private _dialog: MatDialog, private _dialogRef : MatDialogRef<UserHistoryComponent>, @Inject(MAT_DIALOG_DATA) private data : any ) {
  this.userId = data;
  }
  
  ngOnInit(): void {
    this.getUserHistory(); 

    this.paginator._intl.itemsPerPageLabel = 'Lignes par page'; 
    this.paginator._intl.nextPageLabel = 'page suivante';
    this.paginator._intl.previousPageLabel = 'page précédente';
    this.paginator._intl.getRangeLabel = (page: number, pageSize: number, length: number) => {
      if (length === 0 || pageSize === 0) {
        return `0 à ${length }`;
      }
      length = Math.max(length, 0);
      const startIndex = page * pageSize;
      const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;
      return `${startIndex + 1} à ${endIndex} sur ${length}`;
    };     
  }

  /**
  * get user history
  */
  getUserHistory(){
    let historyUrl  = this.serverAdress + Configuration.historyManager.userHistory;
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('access_token')}`
    });
    let body = { 'id': this.userId};
    const stocksHttp$ =  this.RequestManagerService.postRequest(historyUrl, headers, body);
    stocksHttp$
    .subscribe({
      next :(res) => {   
           if(res.success){            
            for(let element of res.content) {
              element.timestamp = element.timestamp.replace('janvier', 'jan.').replace('février', 'fev.')
              .replace('avril', 'avr.').replace('juin', 'jui.').replace('octobre', 'oct.')
              .replace('novembre', 'nov.').replace('décembre', 'dec.');
            }          
            this.dataSource = new MatTableDataSource(res.content);
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;
           }          
      },
       error(err) {
        console.log('HTTP Error', err)
       }
    })
  }

  /**
  * paginator
  * @param event 
  */
  /*applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }*/
}
