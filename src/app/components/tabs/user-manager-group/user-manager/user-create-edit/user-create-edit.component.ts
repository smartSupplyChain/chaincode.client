import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { RequestManagerService } from '../../../../../service/request-manager/request-manager.service'
import { Configuration } from '../../../../../configuration/configuration';
import { HttpHeaders } from '@angular/common/http';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CustomSnackbarComponent } from 'src/app/components/tabs/utils/custom-snackbar/custom-snackbar.component';
@Component({
  selector: 'app-user-edit',
  templateUrl: './user-create-edit.component.html',
  styleUrls: ['./user-create-edit.component.css']
})
export class UserCreateEditComponent implements OnInit{

  dataSource!: MatTableDataSource<any>;
  userForm!: FormGroup;
 
  serverAdress = Configuration.serves_adress.host;
  inprogress = false;
  title : any;
  editMode : boolean = false;
  companies = [null];
  types = [null,'Fournisseur', 'Client', 'Administrateur', 'Visiteur' ];

  constructor(private RequestManagerService : RequestManagerService,
     private fbUser: FormBuilder,
     private _dialogRef : MatDialogRef<UserCreateEditComponent>,
     @Inject(MAT_DIALOG_DATA) private data : any,
     private snackBar : MatSnackBar) {
        this.userForm = this.fbUser.group({
        login: '',
        email: '',
        password: '',
        company: '',
        type: '',
        });
  }

  ngOnInit(): void {
    this.getAllCompanies();
    if(this.data == null){
       this.title = "Création d'utilisateur";
    }
    else {
      this.title = "Modification de l'utilisateur : " + this.data.login;
      this.editMode = true;
    }
    this.userForm.patchValue(this.data);
  }

  /**
  * get all companies
  */
  getAllCompanies(){
    let allCompniesUrl  = this.serverAdress + Configuration.companyManager.getAllCompanies;
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('access_token')}`
    });
    let body = { 'email': localStorage.getItem('email')};
    const companiesHttp$ =  this.RequestManagerService.postRequest(allCompniesUrl, headers, body);
    companiesHttp$
    .subscribe({
      next :(res) => {      
           if(res.success){        
             this.populateCompanies(res.content);
           }          
      },
       error(err) {
        console.log('HTTP Error', err)
       }
    })
  }

  /**
  * populaute user companies
  */
  populateCompanies(res : any) {
    for (var val of res) {
      this.companies.push(val.id);
    } 
  }

  /**
  * create or edit user
  */
  createEditUser(userForm : any) { 
    if(this.editMode){
      this.editUser(userForm);
    }
    else {
      this.createUser(userForm);
    } 
  }

  /**
  * cancel create, edit form
  */
   formCancel() {
    this._dialogRef.close();
  }
  
  /**
   *  create user
   */
  createUser(addUserForm : any) { 
    let login  = addUserForm.value.login;
    let email = addUserForm.value.email;
    let password  = addUserForm.value.password;
    let company  = addUserForm.value.company;
    let type  = addUserForm.value.type;

    if(Boolean(login)  && Boolean(email) && Boolean(password) 
      && Boolean(company) && Boolean(type)){ 
      this.inprogress = true;
      let createUserUrl  = this.serverAdress +  `${Configuration.userManager.createUser}`;
      let headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('access_token')}`
      });
      let body = { 
        'login': login,
        'email': email,
        'password': password,
        'company':company,
        'type': type 
    };
      const createUserHttp$ =  this.RequestManagerService.createRequest(createUserUrl, body, headers);
      createUserHttp$
      .subscribe({
        next :(res) => {      
          this.inprogress = false;

          let isSuccess  = false;
          let panelClassToApply = 'error-message';
          if(res.success){   
            this._dialogRef.close();
            isSuccess = true;
            panelClassToApply = 'success-message';
          }
          
          this.snackBar.openFromComponent(CustomSnackbarComponent,{
            data : {
              message :res.message,
              success : isSuccess
            },
            duration : (5000),
            panelClass : panelClassToApply
          });     
        },
         error(err) {
          console.log('HTTP Error', err);
         }
      })       
    }   
  }

  /**
  *  edit user 
  */
  editUser(addUserForm : any) { //TODO
    let login  = addUserForm.value.login;
    let email = addUserForm.value.email;
    let password  = addUserForm.value.password;
    let company  = addUserForm.value.company;
    let type  = addUserForm.value.type;

    if(Boolean(login)  && Boolean(email) && Boolean(password) 
      && Boolean(company) && Boolean(type)){ 
      this.inprogress = true;
      let editUserUrl  = this.serverAdress +  `${Configuration.userManager.updateUser}`;
      let headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('access_token')}`
      });
      let body = { 
        'login': login,
        'email': email,
        'password': password,
        'company':company,
        'type': type 
    };
      const editUserHttp$ =  this.RequestManagerService.putRequest(editUserUrl, body, headers);
      editUserHttp$
      .subscribe({
        next :(res) => {
          this.inprogress = false;

          let isSuccess  = false;
          let panelClassToApply = 'error-message';
          if(res.success){   
            this._dialogRef.close();
            isSuccess = true;
            panelClassToApply = 'success-message';  
          }
          
          this.snackBar.openFromComponent(CustomSnackbarComponent,{
            data : {
              message :res.message,
              success : isSuccess
            },
            duration : (5000),
            panelClass : panelClassToApply
          });       
        },
         error(err) {
          console.log('HTTP Error', err);
         }
      })       
    }    
  }
  
}