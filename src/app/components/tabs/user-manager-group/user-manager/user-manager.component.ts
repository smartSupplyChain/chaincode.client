import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { RequestManagerService } from '../../../../service/request-manager/request-manager.service'
import { Configuration } from '../../../../configuration/configuration';
import { HttpHeaders } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { UserCreateEditComponent } from './user-create-edit/user-create-edit.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CustomSnackbarComponent } from '../../utils/custom-snackbar/custom-snackbar.component';

@Component({
  selector: 'app-user-manager',
  templateUrl: './user-manager.component.html',
  styleUrls: ['./user-manager.component.css'],
})

export class UserManagerComponent  implements OnInit{
  displayedColumns: string[] = ['Utilisateur', 'Email', 'Entreprise', 'Profil', 'Delete', 'Edit'];
  dataSource!: MatTableDataSource<any>;

  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  inprogress = false;
  serverAdress = Configuration.serves_adress.host;
  currentUser = localStorage.getItem('email');

  constructor(private RequestManagerService : RequestManagerService, private _dialog: MatDialog, private snackBar : MatSnackBar) {
  }
  
  ngOnInit(): void {
    this.getAllUsers(); 

    this.paginator._intl.itemsPerPageLabel = 'Lignes par page'; 
    this.paginator._intl.nextPageLabel = 'page suivante';
    this.paginator._intl.previousPageLabel = 'page précédente';
    this.paginator._intl.getRangeLabel = (page: number, pageSize: number, length: number) => {
      if (length === 0 || pageSize === 0) {
        return `0 à ${length }`;
      }
      length = Math.max(length, 0);
      const startIndex = page * pageSize;
      const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;
      return `${startIndex + 1} à ${endIndex} sur ${length}`;
    };     
  }

  /**
  * get all users
  */
  getAllUsers(){
    let allUsersUrl  = this.serverAdress + Configuration.userManager.getAllusers;
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('access_token')}`
    });
    let body = { 'email': localStorage.getItem('login')};
    const companiesHttp$ =  this.RequestManagerService.postRequest(allUsersUrl, headers, body);
    companiesHttp$
    .subscribe({
      next :(res) => {        
           if(res.success){  
            this.dataSource = new MatTableDataSource(res.content);
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;
           }          
      },
       error(err) {
        console.log('HTTP Error', err)
       }
    })
  }

  /**
  * delete user
  * @param email
  */
  deleteUser(email : any){
    this.inprogress = true;
    let deleteUserUrl  = this.serverAdress + `${Configuration.userManager.deleteUser}${email}`;
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('access_token')}`
    });
    const deleteUserHttp$ =  this.RequestManagerService.deleteRequest(deleteUserUrl, headers);
    deleteUserHttp$
    .subscribe({
      next :(res) => { 
        this.inprogress = false;

        let isSuccess  = false;
        let panelClassToApply = 'error-message';
           if(res.success){           
            this.getAllUsers(); 
            isSuccess = true;
            panelClassToApply = 'success-message';
           }
           
           this.snackBar.openFromComponent(CustomSnackbarComponent,{
            data : {
              message :res.message,
              success : isSuccess
            },
            duration : (5000),
            panelClass : panelClassToApply // color not working
          });          
      },
       error(err) {
        console.log('HTTP Error', err)
       }
    })
  }

  /**
  * open edit user from
  * @param data
  */
  openEditUserForm(data : any){
   const dialogRef = this._dialog.open(UserCreateEditComponent, {data});
   dialogRef.afterClosed().subscribe({
    next:(val) => {
      this.getAllUsers();
    }
   })
  }

  /**
  * open create user from
  */
   openCreateUserForm(){
    const dialogRef = this._dialog.open(UserCreateEditComponent);
    dialogRef.afterClosed().subscribe({
     next:(val) => {
       this.getAllUsers();
     }
    })
   }

  /**
   * paginator
   * @param event 
   */
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}