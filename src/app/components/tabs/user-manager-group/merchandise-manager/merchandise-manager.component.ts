import { Component, ViewChild  } from '@angular/core';
import { MatSort} from '@angular/material/sort';
import { MatTableDataSource} from '@angular/material/table';
import { MatPaginator} from '@angular/material/paginator';
import { RequestManagerService } from '../../../../service/request-manager/request-manager.service'
import { Configuration } from '../../../../configuration/configuration';
import { HttpHeaders } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { MerchandiseCreateEditComponent } from './merchandise-create-edit/merchandise-create-edit.component';
import { CustomSnackbarComponent } from '../../utils/custom-snackbar/custom-snackbar.component';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-merchandise-manager',
  templateUrl: './merchandise-manager.component.html',
  styleUrls: ['./merchandise-manager.component.css']
})
export class MerchandiseManagerComponent {
  displayedColumns: string[] = ['Id', 'Date', 'Delete'];
  dataSource!: MatTableDataSource<any>;

  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  inprogress = false;
  serverAdress = Configuration.serves_adress.host;
  merchandiseToCreate : any;

  constructor(private RequestManagerService : RequestManagerService, private _dialog: MatDialog, private snackBar : MatSnackBar) {}

  ngOnInit(): void {
    this.getMerchandises();  

    this.paginator._intl.itemsPerPageLabel = 'Lignes par page'; 
    this.paginator._intl.nextPageLabel = 'page suivante';
    this.paginator._intl.previousPageLabel = 'page précédente';
    this.paginator._intl.getRangeLabel = (page: number, pageSize: number, length: number) => {
      if (length === 0 || pageSize === 0) {
        return `0 à ${length }`;
      }
      length = Math.max(length, 0);
      const startIndex = page * pageSize;
      const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;
      return `${startIndex + 1} à ${endIndex} sur ${length}`;
    };     
  }

  /**
  * open create merchandise from
  */
  openCreateMerchandiseForm(){
    const dialogRef = this._dialog.open(MerchandiseCreateEditComponent);
    dialogRef.afterClosed().subscribe({
      next:(val) => {
        this.getMerchandises();
    }
    })
  }

  /**
   * get all merchandises
   */
  getMerchandises(){
    let allMerchandiseUrl  = this.serverAdress + Configuration.merchandiseManager.getAllMerchandises;
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('access_token')}`
    });
    let body = { 'email': localStorage.getItem('email')};
    const merchandisesHttp$ =  this.RequestManagerService.postRequest(allMerchandiseUrl, headers, body);
    merchandisesHttp$
    .subscribe({ 
      next :(res) => {      
           if(res.success){       
              this.dataSource = new MatTableDataSource(res.content);
              this.dataSource.sort = this.sort;
              this.dataSource.paginator = this.paginator;
           }          
      },
       error(err) {
        console.log('HTTP Error', err)
       }
    })
  }

  /**
  * delete merchandise
  * @param id
  */
   deleteMerchandise(id : any){
    this.inprogress = true;
    let deletMerchandiseUrl  = this.serverAdress + `${Configuration.merchandiseManager.deleteMerchandise}${id}`;
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('access_token')}`
    });
    const deleteMerchandiseHttp$ =  this.RequestManagerService.deleteRequest(deletMerchandiseUrl, headers);
    deleteMerchandiseHttp$
    .subscribe({
      next :(res) => {
        this.inprogress = false;

        let isSuccess  = false;
        let panelClassToApply = 'error-message';
        if(res.success){        
          this.getMerchandises();
          isSuccess = true;
          panelClassToApply = 'success-message';
        }
        
        this.snackBar.openFromComponent(CustomSnackbarComponent,{
          data : {
            message :res.message,
            success : isSuccess
          },
          duration : (5000),
          panelClass : panelClassToApply
       });             
      },
       error(err) {
        console.log('HTTP Error', err)
       }
    })
  }

  /**
   * paginator
   * @param event 
   */
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
