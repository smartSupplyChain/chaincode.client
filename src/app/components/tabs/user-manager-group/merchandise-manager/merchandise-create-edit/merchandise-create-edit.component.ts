import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { Configuration } from 'src/app/configuration/configuration';
import { RequestManagerService } from 'src/app/service/request-manager/request-manager.service';
import { HttpHeaders } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CustomSnackbarComponent } from 'src/app/components/tabs/utils/custom-snackbar/custom-snackbar.component';

@Component({
  selector: 'app-merchandise-create-edit',
  templateUrl: './merchandise-create-edit.component.html',
  styleUrls: ['./merchandise-create-edit.component.css']
})
export class MerchandiseCreateEditComponent implements OnInit{
  dataSource!: MatTableDataSource<any>;
  merchandiseForm!: FormGroup;

  serverAdress = Configuration.serves_adress.host;
  inprogress = false;
  title : any;
  editMode : boolean = false;

  constructor(private RequestManagerService : RequestManagerService,
    private mrchcompany: FormBuilder,
    private _dialogRef : MatDialogRef<MerchandiseCreateEditComponent>,
    @Inject(MAT_DIALOG_DATA) private data : any,
    private snackBar : MatSnackBar ) {
      this.merchandiseForm = this.mrchcompany.group({
       id: ''
      });
  }

  ngOnInit(): void {
  if(this.data == null){
     this.title = "Création de Marchandises";
  }
  else {
    this.title = "Modification de la marchandise : " + this.data.id;
    this.editMode = true;
  }
  this.merchandiseForm.patchValue(this.data);
  }

  /**
  * create or edit merchandise
  */
  createEditMerchandise(merchandiseForm : any) { 
  if(this.editMode){
    //this.editMerchandise(merchandiseForm);
    console.log("edit user works");
  }
  else {
    this.createMerchandise(merchandiseForm);
  } 
  }
  
  /**
  *  create merchandise
  */
  async createMerchandise(merchandiseForm : any) { 
    let id = merchandiseForm.value.id;
    if(Boolean(id)){
      this.inprogress = true;
      let createMerchandiseUrl  = this.serverAdress +  `${Configuration.merchandiseManager.createMerchandise}`;
      let headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('access_token')}`
      });
      let body = { 'id': id };
      
      const createMerchandiseHttp$ =  this.RequestManagerService.createRequest(createMerchandiseUrl, body, headers);
      createMerchandiseHttp$
      .subscribe({
        next :(res) => {
          this.inprogress = false;

           let isSuccess  = false;
           let panelClassToApply = 'error-message';
          if(res.success){  
            this._dialogRef.close(); 
            isSuccess = true;
            panelClassToApply = 'success-message';
          }
          
          this.snackBar.openFromComponent(CustomSnackbarComponent,{
            data : {
              message :res.message,
              success : isSuccess
            },
            duration : (5000),
            panelClass : panelClassToApply
          });     
        },
         error(err) {
          console.log('HTTP Error', err);
         }
      }) 
    }    
  }

  /**
   *  edit merchandise
   */
  async editMerchandise(merchandiseForm : any) { 
    let id = merchandiseForm.value.id;
    if(Boolean(id)){
      this.inprogress = true;
      let updateMerchandiseUrl  = this.serverAdress +  `${Configuration.merchandiseManager.updateMerchandise}`;
      let headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('access_token')}`
      });
      let body = { 'id': id };
      
      const updateMerchandiseHttp$ =  this.RequestManagerService.createRequest(updateMerchandiseUrl, body, headers);
      updateMerchandiseHttp$
      .subscribe({
        next :(res) => {
          this.inprogress = false;

          let isSuccess  = false;
          let panelClassToApply = 'error-message';
          if(res.success){ 
            this._dialogRef.close();  
            isSuccess = true;
            panelClassToApply = 'success-message';  
          }
          
          this.snackBar.openFromComponent(CustomSnackbarComponent,{
            data : {
              message :res.message,
              success : isSuccess
            },
            duration : (5000),
            panelClass : panelClassToApply // color not working
          });     
        },
         error(err) {
          console.log('HTTP Error', err);
         }
      }) 
    }    
  }

  /**
  * cancel create, edit form
  */
  formCancel() {
    this._dialogRef.close();
  }
}