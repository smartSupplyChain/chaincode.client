import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { Configuration } from 'src/app/configuration/configuration';
import { RequestManagerService } from 'src/app/service/request-manager/request-manager.service';
import { HttpHeaders } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CustomSnackbarComponent } from 'src/app/components/tabs/utils/custom-snackbar/custom-snackbar.component';

@Component({
  selector: 'app-company-create-edit',
  templateUrl: './company-create-edit.component.html',
  styleUrls: ['./company-create-edit.component.css']
})
export class CompanyCreateEditComponent implements OnInit{
  dataSource!: MatTableDataSource<any>;
  companyForm!: FormGroup;
 
  serverAdress = Configuration.serves_adress.host;
  inprogress = false;
  title : any;
  editMode : boolean = false;

  constructor(private RequestManagerService : RequestManagerService,
    private fbcompany: FormBuilder,
    private _dialogRef : MatDialogRef<CompanyCreateEditComponent>,
    @Inject(MAT_DIALOG_DATA) private data : any,
    private snackBar : MatSnackBar ) {
      this.companyForm = this.fbcompany.group({
       id: ''
      });
  }

  ngOnInit(): void {
  if(this.data == null){
     this.title = "Création des Entreprises partenaires du réseau";
  }
  else {
    this.title = "Modification de l'entreprise : " + this.data.id;
    this.editMode = true;
  }
  this.companyForm.patchValue(this.data);
  }

  /**
  * create or edit user
  */
  createEditCompany(companyForm : any) { 
  if(this.editMode){
    //this.editCompany(companyForm);
    console.log("edit user works");
  }
  else {
    this.createCompany(companyForm);
  } 
  }

  /**
   *  create company
   */
  async createCompany(companyForm : any) { 
    let id = this.companyForm.value.id;
    if(Boolean(id)){
      this.inprogress = true;
      let createCompnyUrl  = this.serverAdress +  `${Configuration.companyManager.createCompany}`;
      let headers = new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${localStorage.getItem('access_token')}`
      });
      let body = { 'id': id};
      const createCompanyHttp$ =  this.RequestManagerService.createRequest(createCompnyUrl, body, headers);
      createCompanyHttp$
      .subscribe({
        next :(res) => {  
          this.inprogress = false;

          let isSuccess  = false;
          let panelClassToApply = 'error-message';
          if(res.success){   
            this._dialogRef.close();
            isSuccess = true;
            panelClassToApply = 'success-message';
          }
          
          this.snackBar.openFromComponent(CustomSnackbarComponent,{
            data : {
              message :res.message,
              success : isSuccess
            },
            duration : (5000),
            panelClass : panelClassToApply
          }); 
        },
         error(err) {
          console.log('HTTP Error', err);
         }
      })
    }    
  }

   /**
  *  edit company
  */
   async editCompany(companyForm : any) {  //TODO
    let id = this.companyForm.value.id;
    if(Boolean(id)){
      this.inprogress = true;
      let createCompnyUrl  = this.serverAdress +  `${Configuration.companyManager.updateCompany}`;
      let headers = new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${localStorage.getItem('access_token')}`
      });
      let body = { 'id': id};
      const updateCompanyHttp$ =  this.RequestManagerService.createRequest(createCompnyUrl, body, headers);
      updateCompanyHttp$
      .subscribe({
        next :(res) => {
          this.inprogress = false;

          let isSuccess  = false;
          let panelClassToApply = 'error-message';
          if(res.success){   
            this._dialogRef.close();
            isSuccess = true;
            panelClassToApply = 'success-message';
          }
          
          this.snackBar.openFromComponent(CustomSnackbarComponent,{
            data : {
              message :res.message,
              success : isSuccess
            },
            duration : (5000),
            panelClass : panelClassToApply
          }); 
        },
         error(err) {
          console.log('HTTP Error', err);
         }
      })
    }  
  }

  /**
  * cancel create, edit form
  */
  formCancel() {
    this._dialogRef.close();
  }
   
 
  
}