import { Component, OnInit, ViewChild  } from '@angular/core';
import { MatSort} from '@angular/material/sort';
import { MatTableDataSource} from '@angular/material/table';
import { MatPaginator} from '@angular/material/paginator';
import { HttpHeaders } from '@angular/common/http';
import { RequestManagerService } from '../../../../service/request-manager/request-manager.service'
import { Configuration } from '../../../../configuration/configuration';
import { MatDialog } from '@angular/material/dialog';
import { CompanyCreateEditComponent } from './company-create-edit/company-create-edit.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CustomSnackbarComponent } from '../../utils/custom-snackbar/custom-snackbar.component';

@Component({
  selector: 'app-company-manager',
  templateUrl: './company-manager.component.html',
  styleUrls: ['./company-manager.component.css']
})

export class CompanyManagerComponent  implements OnInit{
  displayedColumns: string[] = ['Id', 'Date', 'Delete'];
  dataSource!: MatTableDataSource<any>;

  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  inprogress = false;
  serverAdress = Configuration.serves_adress.host;
  companyToCreate : any;

  constructor(private RequestManagerService : RequestManagerService, private _dialog: MatDialog, private snackBar : MatSnackBar) {}

  ngOnInit(): void {
    this.getAllCompanies();  

    this.paginator._intl.itemsPerPageLabel = 'Lignes par page'; 
    this.paginator._intl.nextPageLabel = 'page suivante';
    this.paginator._intl.previousPageLabel = 'page précédente';
    this.paginator._intl.getRangeLabel = (page: number, pageSize: number, length: number) => {
      if (length === 0 || pageSize === 0) {
        return `0 à ${length }`;
      }
      length = Math.max(length, 0);
      const startIndex = page * pageSize;
      const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;
      return `${startIndex + 1} à ${endIndex} sur ${length}`;
    };     
  }

  /**
   * get all companies
   */
  getAllCompanies(){
    let allCompniesUrl  = this.serverAdress + Configuration.companyManager.getAllCompanies;
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('access_token')}`
    });
    let body = { 'email': localStorage.getItem('email')};
    const companiesHttp$ =  this.RequestManagerService.postRequest(allCompniesUrl, headers, body);
    companiesHttp$
    .subscribe({
      next :(res) => {      
           if(res.success){        
              this.dataSource = new MatTableDataSource(res.content);
              this.dataSource.sort = this.sort;
              this.dataSource.paginator = this.paginator;
           }          
      },
       error(err) {
        console.log('HTTP Error', err)
       }
    })
  }

   /**
   * delete company
   * @param id
   */
   deleteCompany(id : any){
    this.inprogress = true;
    let deleteCompnyUrl  = this.serverAdress + `${Configuration.companyManager.deleteCompany}${id}`;
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('access_token')}`
    });
    const deleteCompanyHttp$ =  this.RequestManagerService.deleteRequest(deleteCompnyUrl, headers);
    deleteCompanyHttp$
    .subscribe({
      next :(res) => { 
           this.inprogress = false;

           let isSuccess  = false;
           let panelClassToApply = 'error-message';
           if(res.success){   
            this.getAllCompanies();  
            isSuccess = true;
            panelClassToApply = 'success-message';
           }
             
           this.snackBar.openFromComponent(CustomSnackbarComponent,{
            data : {
              message :res.message,
              success : isSuccess
            },
            duration : (5000),
            panelClass : panelClassToApply
          });        
      },
       error(err) {
        console.log('HTTP Error', err)
       }
    })
  }

  /**
  * open create user from
  */
  openCreateCompanyForm(){
    const dialogRef = this._dialog.open(CompanyCreateEditComponent);
    dialogRef.afterClosed().subscribe({
       next:(val) => {
         this.getAllCompanies();
     }
   })
  }

  /**
  * paginator
  * @param event 
  */
  applyFilter(event: Event) {
     const filterValue = (event.target as HTMLInputElement).value;
     this.dataSource.filter = filterValue.trim().toLowerCase();
     if (this.dataSource.paginator) {
       this.dataSource.paginator.firstPage();
     }
   }
  
}