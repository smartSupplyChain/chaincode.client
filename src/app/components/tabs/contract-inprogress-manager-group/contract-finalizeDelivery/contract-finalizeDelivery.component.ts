import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { RequestManagerService } from '../../../../service/request-manager/request-manager.service'
import { Configuration } from '../../../../configuration/configuration';
import { HttpHeaders } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CustomSnackbarComponent } from '../../utils/custom-snackbar/custom-snackbar.component';

@Component({
  selector: 'app-contract-finalizeDelivery',
  templateUrl: './contract-finalizeDelivery.component.html',
  styleUrls: ['./contract-finalizeDelivery.component.css']
})
export class ContractFinalizeDeliveryComponent implements OnInit {
  displayedColumns: string[] = ['Nom contrat', 'Marchandise', 'Quantité', 'Utilisateur', 'Entreprise', 'Fournisseur','EntrepriseFournisseur','Stock','Edit'];
  dataSource!: MatTableDataSource<any>;

  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  inprogress = false;
  serverAdress = Configuration.serves_adress.host;
  currentUser = localStorage.getItem('email');

  constructor(private RequestManagerService : RequestManagerService, private _dialog: MatDialog, private snackBar : MatSnackBar) {
  }
  
  ngOnInit(): void {
    this.getAllContracts(); 
    
    this.paginator._intl.itemsPerPageLabel = 'Lignes par page'; 
    this.paginator._intl.nextPageLabel = 'page suivante';
    this.paginator._intl.previousPageLabel = 'page précédente';
    this.paginator._intl.getRangeLabel = (page: number, pageSize: number, length: number) => {
      if (length === 0 || pageSize === 0) {
        return `0 à ${length }`;
      }
      length = Math.max(length, 0);
      const startIndex = page * pageSize;
      const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;
      return `${startIndex + 1} à ${endIndex} sur ${length}`;
    };     
  }

  /**
  * get all contracts
  */
  getAllContracts(){
    let allContractsUrl  = this.serverAdress + Configuration.contractManager.getAllContracts;
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('access_token')}`
    });
    let body = { 'email': localStorage.getItem('login')};
    const companiesHttp$ =  this.RequestManagerService.postRequest(allContractsUrl, headers, body);
    companiesHttp$
    .subscribe({
      next :(res) => {        
           if(res.success){
            let contracts = [];
            for(let inc = 0 ; inc < res.content.length ; inc ++) {
              if( res.content[inc].status === 'LIVRAISON_EN_COURS') {
                contracts.push( res.content[inc]);
              }
            }
            this.dataSource = new MatTableDataSource(contracts);
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;
           }          
      },
       error(err) {
        console.log('HTTP Error', err)
       }
    })
  }

  sendRequest(data : any){
    this.inprogress = true;
    let editContratUrl  = this.serverAdress + `${Configuration.contractManager.updateContract}`;
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('access_token')}`
    });
  let body = { 
      'id': data.id,
      'userClient': data.userClient,
      'companyClient': data.companyClient,
      'merchandise_id': data.merchandise_id,
      'quantity': data.quantity,
      'userSupplier': data.userSupplier,
      'companySupplier': data.companySupplier,
      'stock_id' : data.stock_id,
      'status': "LIVRAISON_TERMINEE" 
  };
    const editContratHttp$ =  this.RequestManagerService.putRequest(editContratUrl, body, headers);
    editContratHttp$
    .subscribe({
      next :(res) => {
        this.inprogress = false; 

        let isSuccess  = false;
        let panelClassToApply = 'error-message';
        if(res.success){   
          isSuccess = true;
          panelClassToApply = 'success-message';  
        } 
        
        this.snackBar.openFromComponent(CustomSnackbarComponent,{
          data : {
            message :res.message,
            success : isSuccess
          },
          duration : (5000),
          panelClass : panelClassToApply
        }); 
        this.getAllContracts();  
      },
       error(err) {
        console.log('HTTP Error', err);
       }
    })       
  }

  refresh(){
    this.getAllContracts();
  }
  
  /**
   * paginator
   * @param event 
   */
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
