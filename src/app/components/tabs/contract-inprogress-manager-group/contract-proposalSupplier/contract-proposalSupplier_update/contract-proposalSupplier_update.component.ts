import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { RequestManagerService } from '../../../../../service/request-manager/request-manager.service'
import { Configuration } from '../../../../../configuration/configuration';
import { HttpHeaders } from '@angular/common/http';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CustomSnackbarComponent } from 'src/app/components/tabs/utils/custom-snackbar/custom-snackbar.component';

@Component({
  selector: 'app-contract-proposalSupplier_update',
  templateUrl: './contract-proposalSupplier_update.component.html',
  styleUrls: ['./contract-proposalSupplier_update.component.css']
})
export class ContractProposalSupplierUpdateComponent implements OnInit{

  dataSource!: MatTableDataSource<any>;
  contractForm!: FormGroup;
 
  serverAdress = Configuration.serves_adress.host;
  inprogress = false;
  title : any;
  users = [null];
  companies = [null];
  stocks = [null];

  constructor(private RequestManagerService : RequestManagerService,
     private fbUser: FormBuilder,
     private _dialogRef : MatDialogRef<ContractProposalSupplierUpdateComponent>,
     @Inject(MAT_DIALOG_DATA) private data : any,
     private snackBar : MatSnackBar) {
        this.contractForm = this.fbUser.group({
        userSupplier : '',
        companySupplier: '',
        stock_id: ''
        });
  }

  ngOnInit(): void {  
    this.getAllCompanies();
    this.getAllUsers();
    this.getAllStocks();

    this.title = "Contrat : " + this.data.id;
    this.data.userSupplier = localStorage.getItem('login');
    this.data.companySupplier = localStorage.getItem('company');
    this.contractForm.patchValue(this.data);
  }

  getAllCompanies(){
    let allCompniesUrl  = this.serverAdress + Configuration.companyManager.getAllCompanies;
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('access_token')}`
    });
    let body = { 'email': localStorage.getItem('email')};

    const companiesHttp$ =  this.RequestManagerService.postRequest(allCompniesUrl, headers, body);
    companiesHttp$.subscribe({
      next :(res) => {      
           if(res.success){        
            for (var val of res.content) {
              this.companies.push(val.id);
            } 
        }          
      },
       error(err) {
        console.log('HTTP Error', err)
       }
    })
  }

  getAllUsers(){
    let allUsersUrl  = this.serverAdress + Configuration.userManager.getAllusers;
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('access_token')}`
    });
    let body = { 'email': localStorage.getItem('login')};

    const companiesHttp$ =  this.RequestManagerService.postRequest(allUsersUrl, headers, body);
    companiesHttp$.subscribe({
      next :(res) => {      
           if(res.success){    
            for (var val of res.content) {
              this.users.push(val.login);
            } 
         }          
      },
       error(err) {
        console.log('HTTP Error', err)
       }
    })
  }

  getAllStocks(){
    let allStocksUrl  = this.serverAdress + Configuration.stockManager.getAllStocks;
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('access_token')}`
    });
    let body = { 'email': localStorage.getItem('login')};

    const stocksHttp$ =  this.RequestManagerService.postRequest(allStocksUrl, headers, body);
    stocksHttp$.subscribe({
      next :(res) => {      
           if(res.success){            
            for (var val of res.content) {
              this.stocks.push(val.id);
            } 
           }          
      },
       error(err) {
        console.log('HTTP Error', err)
       }
    })
  }

  formCancel() {
    this._dialogRef.close();
  }
  
  updateContract(editContractForm : any) { 
    if( Boolean(editContractForm.value.userSupplier) && 
        Boolean(editContractForm.value.companySupplier) && 
        Boolean(editContractForm.value.stock_id) )
    { 
      this.inprogress = true;
      let editContratUrl  = this.serverAdress + `${Configuration.contractManager.updateContract}`;
      let headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('access_token')}`
      });
      let body = { 
        'id': this.data.id,
        'userClient': this.data.userClient,
        'companyClient': this.data.companyClient,
        'merchandise_id': this.data.merchandise_id,
        'quantity': this.data.quantity,
        'userSupplier': editContractForm.value.userSupplier,
        'companySupplier': editContractForm.value.companySupplier,
        'stock_id': editContractForm.value.stock_id,
        'status' : "PROPOSAL_SUPPLIER" 
      };

      const editContratHttp$ =  this.RequestManagerService.putRequest(editContratUrl, body, headers);
      editContratHttp$.subscribe({
        next :(res) => {
          this.inprogress = false; 

          let isSuccess  = false;
          let panelClassToApply = 'error-message';
          if(res.success){   
            this._dialogRef.close();
            isSuccess = true;
            panelClassToApply = 'success-message';  
          } 
          
          this.snackBar.openFromComponent(CustomSnackbarComponent,{
            data : {
              message :res.message,
              success : isSuccess
            },
            duration : (5000),
            panelClass : panelClassToApply
          });       
        },
         error(err) {
          console.log('HTTP Error', err);
         }
      })       
    }    
  }
}
