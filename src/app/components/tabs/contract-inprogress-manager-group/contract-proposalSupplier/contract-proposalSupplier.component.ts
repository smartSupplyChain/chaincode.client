import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { RequestManagerService } from '../../../../service/request-manager/request-manager.service'
import { Configuration } from '../../../../configuration/configuration';
import { HttpHeaders } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ContractProposalSupplierUpdateComponent } from './contract-proposalSupplier_update/contract-proposalSupplier_update.component';

@Component({
  selector: 'app-contract-proposalSupplier',
  templateUrl: './contract-proposalSupplier.component.html',
  styleUrls: ['./contract-proposalSupplier.component.css']
})
export class ContractProposalSupplierComponent implements OnInit {
  displayedColumns: string[] = ['Nom contrat', 'Marchandise', 'Quantité', 'Utilisateur', 'Entreprise', 'Edit'];
  dataSource!: MatTableDataSource<any>;

  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  serverAdress = Configuration.serves_adress.host;
  currentUser = localStorage.getItem('email');

  constructor(private RequestManagerService : RequestManagerService, private _dialog: MatDialog) {
  }
  
  ngOnInit(): void {
    this.getAllContracts(); 

    this.paginator._intl.itemsPerPageLabel = 'Lignes par page'; 
    this.paginator._intl.nextPageLabel = 'page suivante';
    this.paginator._intl.previousPageLabel = 'page précédente';
    this.paginator._intl.getRangeLabel = (page: number, pageSize: number, length: number) => {
      if (length === 0 || pageSize === 0) {
        return `0 à ${length }`;
      }
      length = Math.max(length, 0);
      const startIndex = page * pageSize;
      const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;
      return `${startIndex + 1} à ${endIndex} sur ${length}`;
    };     
  }

  getAllContracts(){
    let allContractsUrl  = this.serverAdress + Configuration.contractManager.getAllContracts;
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('access_token')}`
    });
    let body = { 'email': localStorage.getItem('login')};
    const companiesHttp$ =  this.RequestManagerService.postRequest(allContractsUrl, headers, body);
    companiesHttp$
    .subscribe({
      next :(res) => {        
           if(res.success){
            let contracts = [];
            for(let inc = 0 ; inc < res.content.length ; inc ++) {
              if( res.content[inc].status === "VERIF_DISPO") {
                contracts.push( res.content[inc]);
              }
            }
            this.dataSource = new MatTableDataSource(contracts);
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;
           }          
      },
       error(err) {
        console.log('HTTP Error', err)
       }
    })
  }

  sendRequest(data : any){
    const dialogRef = this._dialog.open(ContractProposalSupplierUpdateComponent, {data});
    dialogRef.afterClosed().subscribe({
     next:(val) => {
       this.getAllContracts();
     }
    })
  }

  refresh(){
    this.getAllContracts();
  }
  
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
