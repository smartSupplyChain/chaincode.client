import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contract-inprogress-manager-group',
  templateUrl: './contract-inprogress-manager-group.component.html',
  styleUrls: ['./contract-inprogress-manager-group.component.css']
})
export class ContractInProgressManagerGroupComponent implements OnInit {
  type : String = "";
  resetOrEmpty = false;
  checkAvailability = false;
  proposalSupplier = false;
  validClient = false;
  progressDelivery = false;
  finalizeDelivery = false;

  constructor() {}
  
  ngOnInit(): void {
    this.type = String(localStorage.getItem('type')).toUpperCase(); 
    if(this.type === "Fournisseur".toUpperCase()){
      this.checkAvailability = true;
      this.proposalSupplier = true;
      this.progressDelivery = true;
    }
    else if(this.type === "Client".toUpperCase()){
      this.resetOrEmpty = true;
      this.validClient = true;
      this.finalizeDelivery = true;
    }    
  }
}
