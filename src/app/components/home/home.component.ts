import { Component, OnInit } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { RequestManagerService } from '../../service/request-manager/request-manager.service'
import { Configuration } from '../../configuration/configuration';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  userGroupRights = true;
  stockGroupRights = true;
  contractGroupRights = true;
  contractInProgressGroupRights = true;
  traceabilityGroupRights = true;
  distributionGroupRights = true;
  type : String = "";
  leftFooter : any = Configuration.titles.leftFooter;
  rightFooter : any = Configuration.titles.rightFooter;

  serverAdress = Configuration.serves_adress.host;

  channelName: String = "";
  chaincode: String = "";
  networkStatus: String = "";
  duration: String = "";
  blockNumber: String = "";
  organizations: String = "";

  constructor(private RequestManagerService : RequestManagerService) {}
  
  ngOnInit(): void {
    this.type = String(localStorage.getItem('type')).toUpperCase(); 
    if(this.type === "Administrateur".toUpperCase()){
      this.userGroupRights = false;
      this.stockGroupRights = true;
      this.contractGroupRights = true;
      this.contractInProgressGroupRights = true;
      this.traceabilityGroupRights = false;
      this.distributionGroupRights = false;
    }
    if(this.type === "Fournisseur".toUpperCase()){
      this.userGroupRights = true;
      this.stockGroupRights = false;
      this.contractGroupRights = true;
      this.contractInProgressGroupRights = false;
      this.traceabilityGroupRights = false;
      this.distributionGroupRights = false;
    }
    if(this.type === "Client".toUpperCase()){
      this.userGroupRights = true;
      this.stockGroupRights = true;
      this.contractGroupRights = false;
      this.contractInProgressGroupRights = false;
      this.traceabilityGroupRights = false;
      this.distributionGroupRights = false;
    }
    if(this.type === "Visiteur".toUpperCase()){
      this.userGroupRights = true;
      this.stockGroupRights = true;
      this.contractGroupRights = true;
      this.contractInProgressGroupRights = true;
      this.traceabilityGroupRights = false;
      this.distributionGroupRights = false;
    }
    
    setInterval(() => this.getBlockchainInformation(), 5000);
  }

  getBlockchainInformation() {
    let blockchainUrl  = this.serverAdress + Configuration.blockChainManager.blockchain;
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('access_token')}`
    });
    let body = { };
    const stocksHttp$ =  this.RequestManagerService.postRequest(blockchainUrl, headers, body);
    stocksHttp$
    .subscribe({
      next :(res) => {   
            if(res.success){  
              this.networkStatus = "Etat du réseau : " + res.content.networkStatus;
              this.networkStatus = this.networkStatus.replace('Up', 'connecté').replace('Down', 'déconnecté');
              this.duration = "Démarrage du réseau : " + res.content.duration;
              this.duration = this.duration.replace('minutes', ' minutes').replace('hours', ' heures').replace('days', ' jours').replace('Aboutanhour', ' environ 1 heure');
              this.blockNumber = "Nombre de blocs : " + res.content.blockNumber;
              this.channelName = "Nom du canal : " + res.content.channelName;
              this.chaincode = "Nom du chaincode : "+ res.content.chainCodeName;

              this.organizations = "Organisations : [ ";
              for (const [key, value] of Object.entries(res.content.organizations)) {
                this.organizations += " ( " + key + " : " + value + " ) ";
                this.organizations = this.organizations.replace('true', 'active').replace('false', 'inactive');
              }
              this.organizations += "]";
           }          
      },
       error(err) {
        console.log('HTTP Error', err);
       }
    })
  }

}