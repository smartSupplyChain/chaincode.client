import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { Routes,RouterModule } from '@angular/router';
import { AuthGuardService } from '../../service/AuthGuard/auth-guard.service'
import { NavbarComponent } from '../shared/navbar/navbar.component';
import { UserManagerGroupComponent } from '../tabs/user-manager-group/user-manager-group.component';
import { StockManagerGroupComponent } from '../tabs/stock-manager-group/stock-manager-group.component';
import { ContractManagerGroupComponent } from '../tabs/contract-manager-group/contract-manager-group.component';
import { MatTabsModule } from '@angular/material/tabs'; 
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSelectModule } from '@angular/material/select'; 
import { MatTableModule } from '@angular/material/table'; 
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSortModule } from '@angular/material/sort';
import { MatDialogModule } from '@angular/material/dialog';
import { MatPaginatorModule} from '@angular/material/paginator';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { CustomSnackbarComponent}  from '../tabs/utils/custom-snackbar/custom-snackbar.component';
import { CompanyManagerComponent } from '../tabs/user-manager-group/company-manager/company-manager.component';

import { MerchandiseManagerComponent } from '../tabs/user-manager-group/merchandise-manager/merchandise-manager.component';
import { UserManagerComponent } from '../tabs/user-manager-group/user-manager/user-manager.component';
import { StockManagerComponent } from '../tabs/stock-manager-group/stock-manager/stock-manager.component';
import { ContractManagerComponent } from '../tabs/contract-manager-group/contract-manager/contract-manager.component';
import { TraceabilityManagerGroupComponent } from '../tabs/traceability-manager-group/traceability-manager-group.component';
import { TraceabilityAdminManagerComponent} from '../tabs/traceability-manager-group/traceability-admin-manager/traceability-admin-manager.component';
import { TraceabilityStockManagerComponent} from '../tabs/traceability-manager-group/traceability-stock-manager/traceability-stock-manager.component';
import { TraceabilityContractManagerComponent} from '../tabs/traceability-manager-group/traceability-contract-manager/traceability-contract-manager.component';

import { UserCreateEditComponent } from '../tabs/user-manager-group/user-manager/user-create-edit/user-create-edit.component';
import { CompanyCreateEditComponent } from '../tabs/user-manager-group/company-manager/company-create-edit/company-create-edit.component';
import { MerchandiseCreateEditComponent } from '../tabs/user-manager-group/merchandise-manager/merchandise-create-edit/merchandise-create-edit.component';
import { StockCreateEditComponent } from '../tabs/stock-manager-group/stock-manager/stock-create-edit/stock-create-edit.component';
import { ContractCreateEditComponent } from '../tabs/contract-manager-group/contract-manager/contract-create-edit/contract-create-edit.component';
import { UserHistoryComponent } from '../tabs/traceability-manager-group/traceability-admin-manager/user-history/user-history.component';
import { StockHistoryComponent } from '../tabs/traceability-manager-group/traceability-stock-manager/stock-history/stock-history.component';
import { ContractHistoryComponent } from '../tabs/traceability-manager-group/traceability-contract-manager/contract-history/contract-history.component';

import { ContractInProgressManagerGroupComponent } from '../tabs/contract-inprogress-manager-group/contract-inprogress-manager-group.component';
import { ContractCheckAvailabilityComponent } from '../tabs/contract-inprogress-manager-group/contract-checkAvailability/contract-checkAvailability.component';
import { ContractProposalSupplierComponent } from '../tabs/contract-inprogress-manager-group/contract-proposalSupplier/contract-proposalSupplier.component';
import { ContractProposalSupplierUpdateComponent } from '../tabs/contract-inprogress-manager-group/contract-proposalSupplier/contract-proposalSupplier_update/contract-proposalSupplier_update.component';
import { ContractValidClientComponent } from '../tabs/contract-inprogress-manager-group/contract-validClient/contract-validClient.component';
import { ContractProgressDeliveryComponent } from '../tabs/contract-inprogress-manager-group/contract-progressDelivery/contract-progressDelivery.component';
import { ContractFinalizeDeliveryComponent } from '../tabs/contract-inprogress-manager-group/contract-finalizeDelivery/contract-finalizeDelivery.component';
import { ContractReusableComponent } from '../tabs/contract-inprogress-manager-group/contract-reusable/contract-reusable.component';

const routes: Routes = [
  { 
   path:'home',component:HomeComponent,
   canActivate: [AuthGuardService]
  } 
 ];
 
@NgModule({
  declarations: [
    HomeComponent,
    NavbarComponent,
    UserManagerGroupComponent,
    StockManagerGroupComponent,
    ContractManagerGroupComponent,

    CompanyManagerComponent,
    TraceabilityAdminManagerComponent,
    TraceabilityStockManagerComponent,
    TraceabilityContractManagerComponent,
    MerchandiseManagerComponent,
    UserManagerComponent,
    StockManagerComponent,
    ContractManagerComponent,
    TraceabilityManagerGroupComponent,
    UserCreateEditComponent,
    ContractCreateEditComponent,
    UserHistoryComponent,
    StockHistoryComponent,
    ContractHistoryComponent,
    CompanyCreateEditComponent,
    MerchandiseCreateEditComponent,
    StockCreateEditComponent,
    CustomSnackbarComponent,
    ContractInProgressManagerGroupComponent,
    ContractCheckAvailabilityComponent,
    ContractProposalSupplierComponent,
    ContractProposalSupplierUpdateComponent,
    ContractValidClientComponent,
    ContractProgressDeliveryComponent,
    ContractFinalizeDeliveryComponent,
    ContractReusableComponent
  ],
  imports: [
    CommonModule,
    MatTabsModule,
    BrowserModule,
    MatSelectModule,
    MatTableModule,
    MatFormFieldModule,
    MatSortModule,
    MatDialogModule,
    MatPaginatorModule,
    MatInputModule,
    MatButtonModule,
    BrowserAnimationsModule,
    MatIconModule,
    FormsModule,
    ReactiveFormsModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    RouterModule.forChild(routes) 
  ]
})
export class HomeModule { }