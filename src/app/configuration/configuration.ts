export class Configuration {

  static get titles() {
    return {
      title : "Application  Supply Blockchain Ring  1.3",
      subtitle :  "Réseau de partage d’informations entre supply-chain",
      leftFooter :  "Supply Blockchain Ring :  Réseau de partage d’informations entre supply-chain",
      rightFooter :  "Capgemini engineering 2023 – projet SBR",
    };
  }

  // TODO : variables à externaliser dans un fichier de conf
  static get serves_adress() {
    return {
      host :  'http://10.1.2.176:8081' //vm address java
              //'http://10.1.2.176:8080' //vm address intellij
    };
  }

  static get authentication() {
    return {
      auth :   '/blockchain/auth/authenticate'
    
    };
  }

  static get companyManager() {
    return {
      getAllCompanies :   '/blockchain/companyManager/companies',
      createCompany : '/blockchain/companyManager/createCompany', 
      updateCompany : '/blockchain/companyManager/updateCompany',
      deleteCompany : '/blockchain/companyManager/deleteCompany/'
    };
  }

  static get userManager() {
    return {
      userDetails : '/blockchain/userManager/userDetails',
      getAllusers :   '/blockchain/userManager/users',
      createUser : '/blockchain/userManager/createUser', 
      updateUser : '/blockchain/userManager/updateUser', 
      deleteUser : '/blockchain/userManager/deleteUser/'
    };
  }

  static get contractManager() {
    return {
      getAllContracts : '/blockchain/contractManager/contracts',
      createContract : '/blockchain/contractManager/createContract', 
      updateContract : '/blockchain/contractManager/updateContract', 
      deleteContract : '/blockchain/contractManager/deleteContract/'
    };
  }
  
  static get merchandiseManager() {
    return {
      getAllMerchandises: '/blockchain/merchandiseManager/merchandises',
      createMerchandise : '/blockchain/merchandiseManager/createMerchandise',
      updateMerchandise : '/blockchain/merchandiseManager/updateMerchandise', 
      deleteMerchandise : '/blockchain/merchandiseManager/deleteMerchandise/'
    };
  }

  static get stockManager() {
    return {
      getAllStocks :   '/blockchain/stockManager/stocks',
      createStock : '/blockchain/stockManager/createStock', 
      updateStock : '/blockchain/stockManager/updateStock',
      deleteStock : '/blockchain/stockManager/deleteStock/',
    };
  }

  static get historyManager() {
    return {
      userHistory : '/blockchain/userManager/userHistory',
      stockHistory : '/blockchain/stockManager/stockHistory',
      contractHistory : '/blockchain/contractManager/contractHistory'
    };
  }
  
  static get blockChainManager() {
    return {
      blockchain : '/blockchain/blockChainManager/networkDetails'
    };
  }
  
}