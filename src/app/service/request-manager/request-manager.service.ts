import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, catchError, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RequestManagerService {
  constructor(public router: Router, private http: HttpClient) {}
  /**
   * post request
   * @param url 
   * @param body 
   * @param headers 
   * @returns json, List json
   */
  public postRequest(url:string, headers: any, body:any) : Observable<any>{
     return this.http.post(url, body, { headers });
  }

  /**
   * delete request 
   * @param url 
   * @param headers 
   * @returns 
   */
  public deleteRequest(url:string, headers: any) : Observable<any>{
    return this.http.delete(url, {headers});
 }

 /**
   * delete request 
   * @param deleteCompanyUrl 
   * @param headers 
   * @returns 
   */
 public createRequest(url:string, body:any, headers: any) : Observable<any>{
  return this.http.post(url,body ,{headers});
}

/**
   * put request 
   * @param url 
   * @param body
   * @param headers
   * @returns 
   */
public putRequest(url:string, body:any, headers: any) : Observable<any>{
  return this.http.put(url,body ,{headers});
}
}