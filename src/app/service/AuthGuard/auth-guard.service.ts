import { Injectable } from '@angular/core';
import { AuthentificationService } from '../login/authentification.service';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class AuthGuardService {

  constructor(public auth: AuthentificationService, public router: Router) { }

  canActivate(): boolean {
    
    if (!this.auth.isAuthenticated()) {
      this.router.navigate(['']);
      return false;
    }
    return true;
  }
}
