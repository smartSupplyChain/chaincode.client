import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthentificationService {
  constructor(public router: Router, private http: HttpClient) {}

/**
 * get access token from api
 * @param authentUrl 
 * @param headers 
 * @param authentBody 
 * @returns 
 */
  public getAccessToken(authentUrl:string, headers: any, authentBody:any ) : Observable<any>{
     return this.http.post(authentUrl, authentBody, { headers });
  }
  /**
   * check if user still logged-in
   * @returns 
   */
  public isAuthenticated(): boolean {
        if(localStorage.getItem('access_token') == null){
          return false;
        } else {
          return true;
        };  
  }
}