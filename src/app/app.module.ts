import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginModule } from './components/login/login.module';
import { HomeModule } from './components/home/home.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CookieModule } from 'ngx-cookie';
import { ErrorsModule } from './components/errors/errors.module';
import { HttpClientModule } from '@angular/common/http';
import { MatDividerModule} from '@angular/material/divider';


@NgModule({
  declarations: [
    AppComponent
    ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
	  LoginModule,
    HomeModule,
    ErrorsModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    MatDividerModule,
    CookieModule.withOptions()
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }